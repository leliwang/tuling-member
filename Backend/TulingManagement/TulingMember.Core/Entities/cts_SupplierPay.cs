﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace TulingMember.Core
{
    /// <summary>
    /// 。
    /// </summary> 
    [TableAudit]
    public class cts_SupplierPay : DEntityTenant
    {
    
        /// <summary>
        /// 。
        /// </summary>
     
        public string OrderNo { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public DateTime? OrderDate { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public long SupplierId { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public string SupplierName { get; set; }

    
        /// <summary>
        /// 合计。
        /// </summary>
     
        public decimal Amount { get; set; }

        /// <summary>
        /// 优惠
        /// </summary>
        public decimal DiscountAmount { get; set; }
        /// <summary>
        /// 。
        /// </summary>

        public string Remark { get; set; }

    }
}